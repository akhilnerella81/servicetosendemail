
const express = require("express");
const bodyparser = require("body-parser");
const nodemailer = require("nodemailer");

const app = express();

app.use(bodyparser.urlencoded({extended: true}));
// async..await is not allowed in global scope, must use a wrapper
app.use(bodyparser.json());

app.post("/",function(req,res){


  // Generate SMTP service account from ethereal.email

      console.log('Credentials obtained, sending message...');

      // Create a SMTP transporter object
      let transporter = nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: 'hobart.oconner96@ethereal.email',
            pass: '2FKgP4JFD8HJ5fb97N'
        },
      });
      // Message object
      let message = {
          from: 'Sensender@exmple.com>',
          to: req.body.to,
          subject: 'Nodemailer is unicode friendly ✔',
          text: req.body.email_body,

      };

      transporter.sendMail(message, (err, info) => {
          if (err) {
              console.log('Error occurred. ' + err.message);
              return res.status(500).json({"success": "false", "message": err});

          }
           return res.status(200).json({"success": "true", "message": "Email sent successfully"});
          console.log('Message sent: %s', info.messageId);
          // Preview only available when sending through an Ethereal account
          console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
      });
  })








app.listen(3000,function(){
  console.log("Server started at port 3000");
})
